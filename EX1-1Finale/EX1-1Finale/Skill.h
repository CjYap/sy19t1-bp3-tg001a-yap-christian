#pragma once
#include <conio.h>
#include <string>
#include "Character.h"

using namespace std;

class Skill

{
public:
	Skill();
	~Skill();

	void castSkill(Character* player2, Character* player1);

protected:

	string sname;

};

