#pragma once
#include "Skill.h"

class NormalAtk : public Skill
{
public:
	NormalAtk();
	~NormalAtk();

	void castSkill(Character * player2, Character * player1);
	
};

