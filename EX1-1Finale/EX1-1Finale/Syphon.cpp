#include "Syphon.h"
#include <iostream>

using namespace std;

void Syphon::castSkill(Character* player2, Character* player1)
{
	cout << player1->name << " uses Syphon on " << player2->name << endl;
	player2->hp -= player1->weapon->damage;
	float ls = player1->weapon->damage * .25;
	player1->hp += ls;
	cout << player1->name << " heals " << ls << " " << endl;
	cout << player2->name << " remaining hp: " << player2->hp << endl;
	cout << "Your Total HP: " << player1->hp << endl;
}
