#include "Vengence.h"
#include <iostream>

using namespace std;

void Vengence::castSkill(Character* player2, Character* player1)
{
	cout << player1->name << " uses Vengence on " << player2->name << endl;
	float hpRemoved = player1->weapon->damage * .25;
	player1->hp -= hpRemoved;
	player1->weapon->damage = player1->weapon->damage * 2;
	player2->hp -= player1->weapon->damage;
	cout << player2->name << " remaining hp: " << player2->hp;
}
