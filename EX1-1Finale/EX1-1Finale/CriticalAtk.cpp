#include "CriticalAtk.h"
#include <iostream>

using namespace std;

void CriticalAtk::castSkill(Character* player2, Character* player1)
{
	cout << player1->name << " critically attacks " << player2->name << endl;
	player1->weapon->damage = player1->weapon->damage * 2;
	player1->weapon->damage -= player2->hp;
	cout << player2->name << " remaining hp: " << player2->hp << endl;
}
