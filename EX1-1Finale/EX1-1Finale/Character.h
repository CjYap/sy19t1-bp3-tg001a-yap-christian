#pragma once
#include <string>
#include "Weapon.h"
#include <vector>
#include "Skill.h"


using namespace std;
class Skill;
class Character
{
public:
	Character(string name, float hp, float mp, Weapon* weapon);
	~Character();

	void attackPlayer(Character* attacker);
	string name;
	float hp;
	float mp;
	Weapon* weapon;
	vector <Skill*> playerSkill;
};

