#include "Unit.h"

Unit::Unit(string name, int hp, int mp, int pow, int agi, int vit, int dex, string playerCategory)
{
	name = mName;
	hp = mHp;
	mp = mMp;
	pow = mPOW;
	agi = mAGI;
	vit = mVIT;
	dex = mDEX;
	playerCategory = mPlayerCategory;

	mWarriorS.push_back(new BasicAttack("Basic Attack", 0, 1.0));
	mWarriorS.push_back(new Shockwave("Shockwave", 5, 0.9));
	mAssassinS.push_back(new BasicAttack("Basic Attack", 0, 1.0));
	mAssassinS.push_back(new Assassinate("Assassinate", 2.5, 2.2));
	mMageS.push_back(new BasicAttack("Basic Attack", 0, 1.0));
	mMageS.push_back(new Heal("Heal", 10, 0));

}

Unit::~Unit()
{
}

string Unit::getName()
{
	return this->mName;
}

int Unit::getHp()
{
	return this->mHp;
}

int Unit::getMp()
{
	return this->mMp;
}

int Unit::getPOW()
{
	return this->mPOW;
}

int Unit::getAGI()
{
	return this->mAGI;
}

int Unit::getVIT()
{
	return this->mVIT;
}

int Unit::getDEX()
{
	return this->mDEX;
}

void Unit::getBaseDamage(int dC)
{
	int baseDamage = this->getPOW() * dC;
}

int Unit::damage( Unit* target, int dC)
{
	int damage = this->getBaseDamage - target->getVIT();
	return 0;
}

void Unit::heal(Unit* mage, vector<Unit*> teammates)
{


}

void Unit::hitRateSingle(Unit*attacker, Unit* defender)
{

	int hit = (attacker->getDEX / defender->getAGI) * 100;

	

	/*hit% = (DEX of attacker / AGI of defender) * 100

		hit % can never be lower than 20 and higher than 80. This means you need to clamp the result within this range.*/
}

