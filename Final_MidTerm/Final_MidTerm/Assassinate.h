#pragma once
#include "Skill.h"
class Assassinate :
	public Skill
{
public:
	Assassinate(string name, int mpCost, int dC);
	~Assassinate();

	virtual void cast(Unit* attacker, Unit* target, int dC);

};

