#pragma once
#include "Skill.h"

class Heal :
	public Skill
{
public:
	Heal(string name, int mpCost, int dC);
	~Heal();

	virtual void cast(Unit* mage, vector<Unit*> allies);
};

