#include "Shockwave.h"

Shockwave::Shockwave(string name, int mpCost, int dC) : Skill (name, mpCost, dC)
{
}

Shockwave::~Shockwave()
{
}

void Shockwave::cast(Unit* attacker, vector<Unit*> targets, int dC)
{
	cout << attacker->getName << " uses ShockWave on All Units! " << endl;

	for (int i = 0; i <= targets.size(); i++)
	{
		targets[i]->getHp -= attacker->damage(targets[i], dC);
	}
	// warrior vs assassin = damageCoef + .5
}
