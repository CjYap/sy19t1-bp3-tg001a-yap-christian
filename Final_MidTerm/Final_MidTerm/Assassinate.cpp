#include "Assassinate.h"

Assassinate::Assassinate(string name, int mpCost, int dC) : Skill (name, mpCost, dC)
{
}

Assassinate::~Assassinate()
{
}

void Assassinate::cast(Unit* attacker, Unit* target, int dC)
{
	cout << attacker->getName() << " uses Assasinate on " << target->getName() << " !" << endl;
	target->getHp -= attacker->damage(target,dC);

}
