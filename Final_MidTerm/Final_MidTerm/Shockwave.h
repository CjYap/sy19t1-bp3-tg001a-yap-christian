#pragma once
#include "Skill.h"
class Shockwave :
	public Skill
{
public:
	Shockwave(string name, int mpCost, int dC);
	~Shockwave();

	virtual void cast(Unit* attacker, vector<Unit*>targets, int dC);

};

