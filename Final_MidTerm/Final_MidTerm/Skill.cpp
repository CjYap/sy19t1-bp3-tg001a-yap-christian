#include "Skill.h"

Skill::Skill(string name, int mpCost, int dC)
{
	name = mSkillName;
	mpCost = mMpCost;
	dC = mDC;
}

Skill::~Skill()
{
}

string Skill::getSkillName()
{
	return this->mSkillName;
}

int Skill::getMpCost()
{
	return this->mMpCost;
}

int Skill::getDC()
{
	return this->mDC;
}

void Skill::cast(Unit* attacker, vector<Unit*> targets)
{
}
