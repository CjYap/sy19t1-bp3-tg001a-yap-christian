#pragma once
#include <iostream>
#include <conio.h>
#include <vector>
#include"Skill.h"
#include <stdio.h>
#include "BasicAttack.h"
#include "Assassinate.h"
#include "Heal.h"
#include "Shockwave.h"
using namespace std;
class Unit
{
public:
	Unit(string name, int hp, int mp, int pow, int agi, int vit, int dex, string playerCategory);
	~Unit();

	//Units
	enum playerCategory { Warrior = 1, Assassin = 2, Mage = 3 };
	vector<Unit*> player;
	vector <Unit*> enemy;
	


	//getters
	string getName();
	int getHp();
	int getMp();
	int getPOW();
	int getAGI();
	int getVIT();
	int getDEX();

	//functions
	void getBaseDamage(int dC);
	int damage(Unit*attacker, int dc);
	void heal(Unit* mage, vector<Unit*>teammates);
	void display();
	void hitRateSingle(Unit*attacker, Unit*defender);
	

	


private:

	string mName;
	int mHp;
	int mMp;
	int mPOW;
	int mAGI;
	int mVIT;
	int mDEX;
	string mPlayerCategory;
	vector <Skill*> mWarriorS;
	vector <Skill*> mAssassinS;
	vector <Skill*> mMageS;
};

