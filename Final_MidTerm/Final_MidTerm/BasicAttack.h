#pragma once
#include <iostream>
#include <string>
#include "Skill.h"

using namespace std;

class BasicAttack :
	public Skill
{
public:

	BasicAttack(string name, int mpCost, int dC);
	~BasicAttack();

	virtual void cast(Unit* attacker, Unit* target, int dC);
};

