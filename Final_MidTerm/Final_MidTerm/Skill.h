#pragma once
#include <iostream>
#include "Unit.h"

using namespace std;

class Skill
{
public:
	Skill(string name, int mpCost, int dC);
	~Skill();

	string getSkillName();
	int getMpCost();
	int getDC();
	void cast(Unit*attacker, vector<Unit*>targets);

protected:
	string mSkillName;
	int mMpCost;
	int mDC;

};

