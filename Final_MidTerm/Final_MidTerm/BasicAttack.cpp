#include "BasicAttack.h"

BasicAttack::BasicAttack(string name, int mpCost, int dC) : Skill(name, mpCost, dC)
{
	
}

BasicAttack::~BasicAttack()
{
}

void BasicAttack::cast(Unit* attacker, Unit*target, int dC)
{
	int critRand = rand() % 100 + 1;

	cout << attacker->getName() << " uses Basic Attack on " << target->getName() << " !" << endl;

	if (critRand <= 20)
	{
		target->getHp -= attacker->damage(target, dC) + attacker->damage(target, dC) * .20;

	}
	else
		target->getHp -= attacker->damage(target, dC);


}
