/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
using namesapce Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *object = mSceneMgr->createManualObject("object1");

	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	//1st square:Front
	//1st tri
	object->position(-10, 10, 0);
	object->position(-10, -10, 0);
	object->position(10, -10, 0);
	//2ns tri
	object->position(10, -10, 0);
	object->position(10, 10, 0);
	object->position(-10, 10, 0);

	// 2nd square:Back
	//1st tri
	object->position(-10, 10, 10);
	object->position(-10, -10, 10);
	object->position(10, -10, 10);
	//2nd tri
	object->position(10, -10, 10);
	object->position(10, 10, 10);
	object->position(-10, 10, 10);

	//3rd Square: right
	//1st tri
	object->position(10, 10, 0);
	object->position(10, -10, 0);
	object->position(10, -10, 10);
	//2nd tri
	object->position(10, -10, 10);
	object->position(10, 10, 10);
	object->position(10, 10, 0);

	//4th Square: left
	//1st tri
	object->position(-10, 10, 10);
	object->position(-10, -10, 10);
	object->position(-10, -10, 0);
	//2nd tri
	object->position(-10, -10, 0);
	object->position(-10, 10, 0);
	object->position(-10, 10, 10);
	
	//5th Square: Up
	//1st tri
	object->position(-10, 10, 10);
	object->position(-10, 10, 0);
	object->position(10, 10, 0);
	//2nd tri
	object->position(10, 10, 0);
	object->position(10, 10, 10);
	object->position(-10, 10, 10);

	//6th Square: Down
	//1st tri
	object->position(-10, -10, 10);
	object->position(-10, -10, 0);
	object->position(10, -10, 0);
	//2nd tri
	object->position(10, -10, 0);
	object->position(10, -10, 10);
	object->position(-10, -10, 10);





	object->end();
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object)
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
