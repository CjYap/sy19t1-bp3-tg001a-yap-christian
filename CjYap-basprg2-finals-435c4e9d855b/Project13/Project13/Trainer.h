#pragma once
#include <conio.h>
#include <iostream>
#include <string>
#include "Pokemon.h"
#include"PokemonCollection.h"
using namespace std;

class Trainer
{
public:
	Trainer();
	Trainer(string name, char gender, int initialLocationX, int initialLocationY);
	string name;
	char gender;
	int initialLocationX;
	int initialLocationY;
	string mapAndGame(int initialLocationX, int initialLocationY, vector<Pokemon*>wildPokemon, vector <Pokemon*> myPokemon);
	void encounter( vector<Pokemon*> wildPokemon, vector<Pokemon*> myPokemon);
	char answerBattle;
	void pokemonAttack(vector<Pokemon*>pokemon, vector<Pokemon*> myPokemon);

	
	
};

