#include <string>
#include <conio.h>
#include <iostream>
#include "Trainer.h"
#include "Pokemon.h"

using namespace std;


int main()
{

	Trainer *mainChar = new Trainer;

	vector<Pokemon*> wildPokemon;
	Pokemon* Rattata = new Pokemon("Rattata", "Normal", 45, 5, 15);
	wildPokemon.push_back(Rattata);
	Pokemon* Zigzagoon = new Pokemon("Zigzagoon", "Normal", 40, 4, 10);
	wildPokemon.push_back(Zigzagoon);
	Pokemon* Nidoran = new Pokemon("Nidoran", "Poison", 50, 6, 15);
	wildPokemon.push_back(Nidoran);
	Pokemon* Oddish = new Pokemon("Oddish", "Grass", 40, 6, 5);
	wildPokemon.push_back(Oddish);
	Pokemon* Machop = new Pokemon("Machop", "Fighting", 55, 7, 20);
	wildPokemon.push_back(Machop);
	Pokemon* Cleffa = new Pokemon("Cleffa", "Normal", 45, 5, 15);
	wildPokemon.push_back(Cleffa);
	Pokemon* Geodude = new Pokemon("Geodude", "Rock", 55, 7, 20);
	wildPokemon.push_back(Geodude);
	Pokemon* Magby = new Pokemon("Magby", "Fire", 50, 8, 25);
	wildPokemon.push_back(Magby);
	Pokemon* Cacnea = new Pokemon("Cacnea", "Grass", 45, 10, 30);
	wildPokemon.push_back(Cacnea);
	Pokemon* Spearow = new Pokemon("Spearow", "Flying", 50, 7, 25);
	wildPokemon.push_back(Spearow);
	Pokemon* Meowth = new Pokemon("Meowth", "Normal", 55, 11, 30);
	wildPokemon.push_back(Meowth);
	Pokemon* Ralts = new Pokemon("Ralts", "Physic", 45, 7, 25);
	wildPokemon.push_back(Ralts);
	Pokemon* Sandshrew = new Pokemon("Sandshrew", "Sand", 65, 12, 45);
	wildPokemon.push_back(Sandshrew);
	Pokemon* Magnemite = new Pokemon("Magnemite", "Electric", 55, 10, 35);
	wildPokemon.push_back(Magnemite);
	Pokemon* Voltorb = new Pokemon("Voltorb", "Electric", 45, 7, 30);
	wildPokemon.push_back(Voltorb);
	Pokemon* Eevee = new Pokemon("Eevee", "Normal", 50, 11, 40);
	wildPokemon.push_back(Eevee);

	vector<Pokemon*> myPokemon;

	Pokemon* Bulbasaur = new Pokemon("Bulbasaur", "Fire", 50, 5, 25);
	Pokemon* Squirtle = new Pokemon("Squirtle", "Fire", 50, 5, 25);
	Pokemon* Charmander = new Pokemon("Charmander", "Fire", 50, 5, 25);


	int answerFirstPokemon;


	cout << "Hello dear Trainer, my name is Professor Oak." << endl;
	cout << "Today we start your adventure towards becoming a pokemon master." << endl;
	cout << endl;
	cout << "But first let us start with your name. What is your name Trainer:";
	cin >> mainChar->name;
	cout << endl;
	cout << "Are you a female or a male? Answer [f] or [m]" << endl;
	cin >> mainChar->gender;

	if (mainChar->gender == 'f')
	{
		cout << " Hello Ms. " << mainChar->name << endl;
		cout << " Welcome to the Word of Pokemon!" << endl;
	}
	if (mainChar->gender == 'm')
	{

		cout << " Hello Mr. " << mainChar->name << endl;
		cout << " Welcome to the Word of Pokemon!" << endl;
	}

	cout << "Now let us choose your first Pokemon: " << endl;

	cout << " [1] Charmander: a fire type Pokemon." << endl;
	cout << " [2] Squirtle: a water type Pokemon." << endl;
	cout << " [3] Bulbasaur: a grass type Pokemon. " << endl;

	cin >> answerFirstPokemon;
	cout << endl;

	if (answerFirstPokemon == 1)
	{
		
		myPokemon.push_back(Charmander);

		cout << "You have gained Charmander." << endl;
	
	}

	else if (answerFirstPokemon == 2)
	{

		myPokemon.push_back(Squirtle);

		cout << "You have gained Squirtle." << endl;

		

	}
	else if (answerFirstPokemon == 3)
	{

		myPokemon.push_back(Bulbasaur);

		cout << "You have gained Bulbasaur." << endl;
		

		
	
	}

	cout << "Name: "<< myPokemon.at(0)->pokemonName << endl;
	cout <<"Type: "<< myPokemon.at(0)->type << endl;
	cout <<"Level "<<myPokemon.at(0)->initialLevel << endl;

	
	
	_getch();

	system("CLS");
	
	cout << "Now you have gained your first Pokemon and it's your turn to start you Adventure." << endl;
	mainChar->initialLocationX = 0;
	mainChar->initialLocationY = 0;


	mainChar->mapAndGame(mainChar->initialLocationX, mainChar->initialLocationY, wildPokemon, myPokemon);
	
	//mainChar->pokemonAttack(wildPokemon, myPokemon);

	//for (int i = 0; i >= 0; i++)
	//{
	//	mainChar->encounter(wildPokemon, myPokemon); // TRY POKEMON ENCOUNTER

	//}
	
	_getch();

	return 0;
}