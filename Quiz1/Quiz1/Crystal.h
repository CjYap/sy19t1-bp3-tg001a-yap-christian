#pragma once
#include "Item.h"
class Crystal :
	public Item
{
public:
	Crystal(string name, int rarityPt);
	~Crystal();

	virtual void addEffects(Character*player);
};

