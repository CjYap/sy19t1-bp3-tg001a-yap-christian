#pragma once
#include "Item.h"
class R :
	public Item
{
public:
	R(string name, int rarityPt);
	~R();
	virtual void addEffects(Character*player);
};

