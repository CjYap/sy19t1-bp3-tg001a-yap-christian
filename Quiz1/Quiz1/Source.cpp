#include <iostream>
#include <conio.h>
#include <string>
#include "Character.h"

using namespace std;

int main()
{
	Character * player = new Character("Sir Kevin", 100, 0, 0);

	while (player->alive())
	{
		player->display(player);
		player->game(player);
		system("pause");
		system("cls");
	}
	return 0;
}