#pragma once
#include "Item.h"
class SR :
	public Item
{
public:
	SR(string name, int rarityPt);
	~SR();

	virtual void addEffects(Character*player);

};

