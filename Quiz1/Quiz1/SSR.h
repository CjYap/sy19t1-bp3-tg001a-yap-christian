#pragma once
#include "Item.h"
class SSR :
	public Item
{
public:
	SSR(string name, int rarityPt);
	~SSR();

	virtual void addEffects(Character*player);
};

