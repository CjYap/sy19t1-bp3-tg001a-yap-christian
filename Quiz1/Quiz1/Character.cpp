#include "Character.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"
#include "HP.h"
#include "Bomb.h"
#include "Crystal.h"
#include <iostream>

using namespace std;



Character::Character(string name, int hp, int totalR, int totalCrystal)
{
	name = name;
	hp = hp;
	totalR = totalR;
	totalCrystal = totalCrystal;
	items.push_back(new R("R", 1));
	items.push_back(new SR("SR", 10));
	items.push_back(new SSR("R", 50));
	items.push_back(new HP("HP", 0));
	items.push_back(new Bomb("Bomb", 0));
	items.push_back(new Crystal("Crystal", 0));

}

Character::~Character()
{
}

void Character::takedamage(Character * player)
{
	this->hp = this->hp - 25;
}

void Character::heal(Character * player)
{
	this->hp = this->hp + 30;
}

void Character::addSSR(Character * player)
{
	player->totalRarity = player->totalRarity + 50;
}

void Character::addSR(Character * player)
{
	player->totalRarity = player->totalRarity + 10;
}

void Character::addR(Character * player)
{
	player->totalRarity = player->totalRarity + 1;
}

void Character::addCrystal(Character * player)
{
	player->totalCrystal = player->totalCrystal + 15;
}

void Character::minusCrystal(Character * player)
{
	player->totalCrystal = player->totalCrystal - 5;
}

int Character::alive()
{

	if (hp > 0)
	{
		return true;
	}
	else
		return false;
	
}

void Character::display(Character * player)
{
	cout << "Character: " << player->name << endl;
	cout << "HP: " << player->hp<< endl;
	cout << "Rarity Points: " << player->totalR << endl;
	cout << "Crystals: " << player->totalCrystal << endl;
	
}

void Character::game(Character * player)
{

	int itemR = rand() + 100;
	player->minusCrystal;
	
	
	if (itemR <= 1)
	{
		player->items[2];
	}
	if (itemR > 1 || itemR <= 10)
	{
		player->items[1];
	}
	if (itemR > 10 || itemR <= 50)
	{
		player->items[0];
	}
	if (itemR > 50 || itemR <= 55)
	{
		player->items[3];
	}
	if (itemR > 55 || itemR <= 70)
	{
		player->items[4];
	}
	if (itemR > 70 || itemR <= 85)
	{
		player->items[5];
	}
}
