#pragma once
#include "Item.h"
class Bomb :
	public Item
{
public:
	Bomb(string name, int rarityPt);
	~Bomb();

	virtual void addEffects(Character*player);
};

