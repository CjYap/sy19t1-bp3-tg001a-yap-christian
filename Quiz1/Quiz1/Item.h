#pragma once
#include "Character.h"
#include <string>

class Item
{
public:
	Item(string name, int rarityPt);
	~Item();

	string itemName();
	int getRarity();
protected:
	string mainName;
	int mainPt;
};

