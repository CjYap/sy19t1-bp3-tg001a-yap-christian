#pragma once
#include <string>
#include "Item.h"
#include <vector>


using namespace std;
class item;
class Character
{
public:
	Character(string name, int hp, int totalR, int totalCrystal);
	~Character();

	string name;
	int hp;
	int totalRarity;
	int totalR;
	int totalCrystal;

	vector <Item*> items;
	
	void takedamage(Character *player);
	void heal(Character *player);
	void addSSR(Character*player);
	void addSR(Character*player);
	void addR(Character*player);
	void addCrystal(Character*player);
	void minusCrystal(Character*player);

	int alive();
	void display(Character*player);
	void game(Character*player);



};

