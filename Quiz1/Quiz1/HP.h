#pragma once
#include "Item.h"
class HP :
	public Item
{
public:
	HP(string name, int rarityPt);
	~HP();
	virtual void addEffects(Character*player);
};

