#include "Character.h"
#include <string>
#include <conio.h>
#include <iostream>
#include <vector>

using namespace std;


Character::Character(string name, float hp, float mp, Weapon * weapon)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
	this->weapon = weapon;

	charskills.push_back(new NormalAttack("Normal"));
	charskills.push_back(new CriticalAttack("Critical"));
	charskills.push_back(new Vengence("Vengence"));
	charskills.push_back(new Syphon("Syphon"));

}

Character::~Character()
{
}

void Character::attackPlayer(Character * player,Character * player2)
{
	int skillR = rand() % charskills.size();

	Skill* useSkill = charskills[skillR];
	useSkill->castSkill(this, player2);


}


