#include <conio.h>
#include <iostream>
#include <string>
#include "Character.h"
#include "Weapon.h"



using namespace std;

int main()
{
	Weapon* WeaponOne = new Weapon("Long Sword", 15);
	Weapon* WeaponTwo = new Weapon("Katana", 15);

	Character* PlayerOne = new Character("James", 50, 20, WeaponOne );
	Character* PlayerTwo = new Character("Lance", 50, 20, WeaponTwo);


	while (PlayerOne->hp || PlayerTwo->hp != 0)
	{

		PlayerOne->attackPlayer(PlayerOne, PlayerTwo);
		PlayerTwo->attackPlayer(PlayerTwo, PlayerOne);

		system("Pause");
	}

	_getch();

	return 0;
}
