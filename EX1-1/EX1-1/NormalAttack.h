#pragma once
#include <iostream>
#include "Skill.h"

using namespace std;

class NormalAttack : public Skill
{
public:
	NormalAttack(string name);
	~NormalAttack();

	void castSkill(Character*player, Character*player2);
};

