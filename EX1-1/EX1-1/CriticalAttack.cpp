#include "CriticalAttack.h"
#include <iostream>

using namespace std;



CriticalAttack::CriticalAttack(string name) : Skill (name)
{
}


CriticalAttack::~CriticalAttack()
{
}

void CriticalAttack::castSkill(Character * player, Character * player2)
{
	cout << player->name << " critically attacks " << player->name << endl;
	player->weapon->damage = player->weapon->damage * 2;
	player->weapon->damage -= player2->hp;
	cout << player2->name << " remaining hp: " << player2->hp << endl;
}
