#pragma once
#include <string>

using namespace std;

class Weapon
{
public:
	Weapon(string name, float damage);
	~Weapon();
	string name;
	int damage;
};

