#pragma once
#include <string>
#include "Weapon.h"
#include "Skill.h"
#include <vector>
#include "NormalAttack.h"
#include "CriticalAttack.h"
#include"Syphon.h"
#include "Vengence.h"

using namespace std;

class Character
{
public:
	Character(string name, float hp, float mp, Weapon* weapon);
	~Character();
	
	string name;
	float hp;
	float mp;
	Weapon*weapon;
	void attackPlayer(Character* attacker, Character* enemy);

	vector <Skill*> charskills;

	

	
};

