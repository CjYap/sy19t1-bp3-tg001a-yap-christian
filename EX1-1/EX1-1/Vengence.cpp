#include "Vengence.h"
#include <iostream>

using namespace std;


Vengence::Vengence(string name) : Skill (name)
{
}


Vengence::~Vengence()
{
}

void Vengence::castSkill(Character * player, Character * player2)
{
	cout << player->name << " uses Vengence on " << player2->name << endl;
	int hpRemoved = player->weapon->damage * .25;
	player->hp -= hpRemoved;
	player->weapon->damage = player->weapon->damage * 2;
	player2->hp -= player->weapon->damage;
	cout << player2->name << " remaining hp: " << player2->hp;
}
