#pragma once
#include <string>
#include "Skill.h"
class CriticalAttack:public Skill
{
public:
	CriticalAttack(string name) ;
	~CriticalAttack();

	void castSkill(Character* player, Character *player2);
};

