#pragma once
#include <string>
#include "Character.h"


class Skill
{
public:
	Skill(string name);
	~Skill();

	string ogName();

	virtual void castSkill(Character * player, Character * player2);

protected:
	string name;


};

