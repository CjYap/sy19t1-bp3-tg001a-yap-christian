#include "Syphon.h"
#include <iostream>
#include"Skill.h"
using namespace std;


Syphon::Syphon(string name) : Skill (name)
{

}


Syphon::~Syphon()
{
}

void Syphon::castSkill(Character * player, Character * player2)
{
	cout << player->name << " uses Syphon on " << player2->name << endl;
	player2->hp -= player->weapon->damage;
	int ls = player->weapon->damage * .25;
	player->hp += ls;
	cout << player->name << " heals " << ls << " " << endl;
	cout << player2->name << " remaining hp: " << player2->hp << endl;
	cout << "Your Total HP: " << player->hp << endl;

}
