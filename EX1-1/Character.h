#pragma once
#include <string>
#include "Weapon.h"


using namespace std;

class Character
{
public:
	Character(string name, float hp, float mp, Weapon* weapon);
	~Character();
	
	string name;
	float hp;
	float mp;
	Weapon*weapon;
	void attackPlayer(Character* attacker);

};

