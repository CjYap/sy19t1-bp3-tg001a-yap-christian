#include "NormalAttack.h"
#include <iostream>

using namespace std;



NormalAttack::NormalAttack()
{
}


NormalAttack::~NormalAttack()
{
}

void NormalAttack::castSkill(Character * player, Character * player2)
{
	cout << player->name << " attacks " << player2->name << endl;
	player2->hp -= player->weapon->damage;
	cout << player2->name << " remaining hp: " << player2->hp << endl;
}
