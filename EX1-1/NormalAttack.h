#pragma once
#include "Skill.h"

class NormalAttack : public Skill
{
public:
	NormalAttack();
	~NormalAttack();

	void castSkill(Character*player, Character*player2);
};

