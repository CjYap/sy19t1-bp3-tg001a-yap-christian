#include "Syphon.h"
#include <iostream>

using namespace std;


Syphon::Syphon()
{
}


Syphon::~Syphon()
{
}

void Syphon::castSkill(Character * player, Character * player2)
{
	cout << player->name << " uses Syphon on " << player2->name << endl;
	player2->hp -= player->weapon->damage;
	float ls = player->weapon->damage * .25;
	player->hp += ls;
	cout << player->name << " heals " << ls << " " << endl;
	cout << player2->name << " remaining hp: " << player2->hp << endl;
	cout << "Your Total HP: " << player->hp << endl;

}
