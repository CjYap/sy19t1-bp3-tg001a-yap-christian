#include "Character.h"
#include <string>
#include <conio.h>
#include <iostream>

using namespace std;


Character::Character(string name, float hp, float mp, Weapon * weapon)
{
	this->name = name;
	this->hp = hp;
	this->mp = mp;
	this->weapon = weapon;
}

Character::~Character()
{
}

void Character::attackPlayer(Character * player)
{
	int skillR = rand() % 5 + 1;

	if (skillR == 1)
	{
		cout << this->name << " attacks " << player->name << endl;
		player->hp -= this->weapon->damage;
		cout << player->name << " remaining hp: " << player->hp <<  endl;
		
	}
	else if (skillR == 2)
	{
		cout << this->name << " critically attacks " << player->name << endl;
		this->weapon->damage = weapon->damage * 2;
		this->weapon->damage -= player->hp;
		cout << player->name << " remaining hp: " << player->hp << endl;

	}

	else if (skillR == 3)
	{
		cout << this->name << " uses Syphon on " << player->name << endl;
		player->hp -= this->weapon->damage;
		float ls =  this->weapon->damage * .25;
		this->hp += ls;
		cout << this->name << " heals " << ls << " " << endl;
		cout << player->name << " remaining hp: " << player->hp << endl;
		cout << "Your Total HP: " << this->hp << endl;


	}

	else if (skillR == 4)
	{
		cout << this->name << " uses Vengence on " << player->name << endl;
		float hpRemoved = this->weapon->damage * .25;
		this->hp -= hpRemoved;
		this->weapon->damage = this->weapon->damage * 2;
		player->hp -= this->weapon->damage;
		cout << player->name << " remaining hp: " << player->hp;
	}
}
